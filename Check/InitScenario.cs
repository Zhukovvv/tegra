﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Включить префабы согласно полученным данным из ScenaStorage
/// </summary>
public class InitScenario : MonoBehaviourSingleton<InitScenario>
{
    //Список префабов сценариев
    public GameObject[] ScenarioPrefabList = null;
    public static Action updateScenarioPrefab = null;

    private void Awake()
    {
        updateScenarioPrefab += UpdatePrefabScenario;
    }

    private void OnEnable()
    {
        UpdatePrefabScenario();

        //Инициализировать очередь выполненых активностей
        ActivitySaver.Instance.InitActivityData(SceneManager.GetActiveScene().name);

        //Перегрузить очередь выполненых активностей
        ActivityReloader.RunReload();
    }

    public void UpdatePrefabScenario()
    {
        //Обновить данные для сцены. (Изменить активный квест)
        ScenaController.UpdateScenaData?.Invoke();

        string prefabId = ScenaStorage.GetDataToStorage(SceneManager.GetActiveScene().name);

        if (prefabId != null)
        {
            LoadScenarioPrefab(prefabId);
        }
        else
        {
            prefabId = GetFirstScenario(SceneManager.GetActiveScene().name);
            LoadScenarioPrefab(prefabId);
        }

        ScenaStorage.SaveDataToStorage(SceneManager.GetActiveScene().name, prefabId);
    }

    /// <summary>
    /// Получить первый активный сценарий
    /// </summary>
    public string GetFirstScenario<T>(T locationName)
    {
        return ScenaData.Instance.GetFirstScenario(locationName.ToString());
    }

    public GameObject GetActiveScenario()
    {
        return null;
    }

    /// <summary>
    /// Загрузить сценарий адитивно 
    /// </summary>
    /// <param name="scenarioName"></param>
    private void LoadScenarioPrefab(string prefabId)
    {
        if (SceneManager.GetSceneByName(prefabId).name != prefabId)
            try { SceneManager.LoadScene(prefabId, LoadSceneMode.Additive); } catch (Exception) { }
    }
}
