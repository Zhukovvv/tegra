﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для работы с данными dailyquestlist
    /// </summary>
    public class DailyQuestToJson : Singleton<DailyQuestToJson>, IJson
    {
        private GetListToResorses<DailyQuests> GetListToResorses { get; }
        private List<DailyQuests> dailyquesttable = new List<DailyQuests>();
        private JsonNode jsonNode;
        private string listName = "Collect";

        public DailyQuestToJson()
        {
            dailyquesttable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.dailyquestlist);
        }

        public  List<DailyQuests> GetItemTable()
        {
            return dailyquesttable;
        }

    }

}
