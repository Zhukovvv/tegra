﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using SimpleJSONMetaSheets;
using System.Linq;


namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла eventlist
    /// </summary>
    public class EnemyWaveToJson<T> where T : new()
    {
        JsonNode jsonNode;
        GetListToEvent<T> geListToEvent;

        private static EnemyWaveToJson<T> instance;
        public static EnemyWaveToJson<T> Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EnemyWaveToJson<T>();
                }
                return instance;
            }
        }

        public JSONNode GetJsonData(string listName)
        {
            return jsonNode.GetJsonNode(listName);
        }

        public ArrayList MakeEventTable(string sheetName, string listName)
        {
            var jsonNode = GetJsonData(listName);

            var nodeList = jsonNode[sheetName.ToString()];

            geListToEvent.InitRowTable(nodeList);

            var eventtable = geListToEvent.MakeEventTable();

            return eventtable;
        }
    }
}
