﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла fpslist
    /// </summary>
    public class FpsToJson : Singleton<FpsToJson>, IJson
    {
        private GetListToResorses<FpsRow> GetListToResorses { get; }
        private List<FpsRow> fpstable = new List<FpsRow>();
        private JsonNode jsonNode;
        private string listName = "Fps";

        public FpsToJson()
        {
            fpstable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.fpslist);
        }

        //Найти fps по Id
        public FpsRow GetFpsConfiguration(string fpsID)
        {
            FpsRow qwery = new FpsRow();

            try
            {
                qwery = (from FpsRow fpsRow in fpstable where fpsRow.ITEM_ID == fpsID select fpsRow).First();
                return qwery;
            }
            catch (Exception e)
            {
                //TODO Добавить обработчик
               Debug.Log("Ошибка FPS ID to table fpslist (ITEM_ID not found)" + e);
            }

            return qwery;
        }
    }
}
