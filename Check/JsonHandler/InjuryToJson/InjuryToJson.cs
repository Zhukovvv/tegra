﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла injurylist
    /// </summary>
    public class InjuryToJson : Singleton<InjuryToJson>, IJson
    {
        private GetListToResorses<InjuryRow> GetListToResorses { get; }
        private List<InjuryRow> injurytable = new List<InjuryRow>();
        private JsonNode jsonNode;
        private string listName = "Injury";

        public InjuryToJson()
        {
            injurytable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.injurylist);
        }

        public List<InjuryRow> GetInjuryTable()
        {
            return injurytable;
        }
    }
}
