﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

/// <summary>
/// Класс для получения данных с файла itemlist
/// </summary>
namespace JsonConvertor
{
    public class ItemToJson : Singleton<ItemToJson>, IJson
    {
        private GetListToResorses<ItemRow> GetListToResorses { get; }
        private List<ItemRow> itemtable = new List<ItemRow>();
        private JsonNode jsonNode;
        private string listName = "Item";

        public ItemToJson()
        {
            itemtable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.itemlist);
        }

        public List<ItemRow> GetItemTable()
        {
            return itemtable;
        }
    }
}
