﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using SimpleJSONMetaSheets;
using UnityEngine;

/// <summary>
/// Структуры JSON
/// </summary>
namespace JsonConvertor
{
    /// <summary>
    /// Список Json Файлов
    /// </summary>
    public struct JsonList
    {
        public static string fpslist = "fpslist";
        public static string translatelist = "translatelist";
        public static string itemlist = "itemlist";
        public static string questlist = "questlist";
        public static string recipeslist = "recipeslist";
        public static string screenplaylist = "screenplaylist";
        public static string shoplist = "shoplist";
        public static string dailyquestlist = "dailyquestlist";
        public static string lootlist = "lootlist";
        public static string destructrecipeslist = "destructrecipeslist";
        public static string uprecipeslist = "uprecipeslist";
        public static string repairrecipeslist = "repairrecipeslist";
        public static string caselist = "caselist";
        public static string finallist = "finallist";
        public static string wavelist = "wavelist";
        public static string portallist = "portallist";
        public static string injurylist = "injurylist";
        public static string keylist = "keylist";
        public static string locationslist = "locationslist";
        public static string scenelist = "scenelist";
        public static string reactionlist = "reactionlist";

        //Листы юнитов
        public static string unitlist = "unitlist";
        public static string skilllist = "skilllist";
        public static string equipmentlist = "equipment";
    };

    #region HELPER
    /// <summary>
    /// Интерфейс класса данных для получения нод с листа
    /// </summary>
    interface IJson
    {
        JSONNode GetJsonData();
    }

    public struct TClass<T> where T : new()
    {
        public T GetObject()
        {
            return new T();
        }
    }

    public struct EnumerableSequense
    {
        public IEnumerable<JSONNode> EventSequence(JSONNode jSONNode)
        {
            // Yield even numbers in the range.
            for (int number = 0; number <= jSONNode.Count - 1; number++)
            {
                yield return jSONNode[number];
            }
        }
    }

    public struct JsonNode
    {
        public TextAsset GetJsonTextAsset(string listName)
        {
            string filepath = "Json/" + listName;
            var jsonTextAssets = Resources.Load<TextAsset>(filepath);
            return jsonTextAssets;
        }

        public JSONNode GetJsonNode(string listName)
        {
            TextAsset textAsset = GetJsonTextAsset(listName);
            var node = JSON.Parse(textAsset.text);
            return node;
        }
    }

    public class Element
    {
        public string tableName { get; }

        Dictionary<string, string> _dictionary { get; set; }

        public Element(string _tableName)
        {
            tableName = _tableName;
            _dictionary = new Dictionary<string, string>();
        }

        public void SetElement(Dictionary<string, string> dictionary)
        {
            _dictionary = dictionary;
        }

        public Dictionary<string, string> GetElement()
        {
            return _dictionary;
        }
    }
    #endregion

    /// <summary>
    /// Получить данные с листов ресурсов (Горизонтальные таблицы)
    /// </summary>
    public struct GetListToResorses<T> where T : new()
    {
        TClass<T> TClass;
        EnumerableSequense enumerableSequense;

        public List<T> GetSheetToList(JSONNode jsonNode, string listName)
        {
            List<T> rowTable = new List<T>();

            foreach (var node in enumerableSequense.EventSequence(jsonNode[listName]))
            {
                var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

                var TObject = TClass.GetObject();

                foreach (var field in fields)
                {
                    if (field != null)
                    {
                        string fieldName = field.Name.ToString();
                        string nodeData = node[fieldName];

                        field.SetValue(TObject, nodeData);
                    }
                }

                rowTable.Add(TObject);
            }

            return rowTable;
        }

        /// <summary>
        /// Получить список листов через ноду
        /// </summary>
        /// <param name="jsonNode"></param>
        /// <returns></returns>
        public List<string> GetSheetListName(JSONNode jsonNode)
        {
            var configNodeList = JsonConvert.DeserializeObject<Dictionary<string, ArrayList>>(jsonNode.ToString());
            return configNodeList.Keys.ToList();
        }
    }

    /// <summary>
    /// Получить данные с листов событий (вертикальные таблицы)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct GetListToEvent<T> where T : new()
    {
        public TClass<T> TClass;

        EnumerableSequense enumerableSequense;
        public ArrayList rowTable { get; set; }

        public void InitRowTable(JSONNode jsonNode)
        {
            rowTable = new ArrayList();

            IEnumerable<JSONNode> nodes = enumerableSequense.EventSequence(jsonNode);

            //Выбрать поля в таблицу
            foreach (var nodeChildList in nodes)
            {
                var rowList = JsonConvert.DeserializeObject<Dictionary<string, string>>(nodeChildList.ToString());

                string nodeName = nodeChildList[0];

                Element element = new Element(nodeName);

                element.SetElement(rowList);

                rowTable.Add(element);
            }
        }


        //Получить список волн из таблицы
        public Dictionary<string, string> GetWave()
        {
            foreach (Element waveColumn in rowTable)
            {
                if (waveColumn.tableName.Contains("WaveTime"))
                {
                    return waveColumn.GetElement();
                }
            }

            return null;
        }

        /// <summary>
        /// Получить список 
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="waveName"></param>
        /// <param name="rowSettingsForTable"></param>
        /// <returns></returns>
        public ArrayList GetSpawnerList(string waveName)
        {
            ArrayList spawnerStor = new ArrayList();

            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

            var TObject = TClass.GetObject();

            int counter = 1;

            foreach (Element element in rowTable)
            {
                var field = (from FieldInfo fieldInfo in fields where fieldInfo.Name.Contains(element.tableName) select fieldInfo).First();

                var value = element.GetElement();

                if (value.ContainsKey(waveName))
                {
                    string waveConfig = value[waveName];

                    field.SetValue(TObject, waveConfig);

                    if ((fields.Length - 1) <= counter)
                    {
                        spawnerStor.Add(TObject);
                        TObject = TClass.GetObject();
                        counter = 1;
                    }
                }
                counter++;
            }

            return spawnerStor;
        }

        public ArrayList MakeEventTable()
        {
            var waveList = GetWave();

            ArrayList eventTable = new ArrayList();

            foreach (var wave in waveList)
            {
                if (wave.Key != "WaveName")
                {
                    //Добавляем конфигурацию волны
                    eventTable.Add(new ArrayList() { wave.Key, wave.Value, GetSpawnerList(wave.Key) });
                }
            }

            return eventTable;
        }
    }

    /// <summary>
    /// Получить данные для юнитов
    /// </summary>
    /// 
    public struct GetUnitConfig
    {
        public ArrayList SelectRowList(string sheetName, Dictionary<string, ArrayList> skillTable)
        {
            var skillConfig = skillTable[sheetName];

            ArrayList rowList = new ArrayList();

            Dictionary<string, string> dictionaryTable = new Dictionary<string, string>();

            foreach (var node in skillConfig)
            {
                int column1 = 0;
                int column2 = 1;

                var configNodeList = JsonConvert.DeserializeObject<Dictionary<string, string>>(node.ToString());
                var lconfigNodeList = configNodeList.Select(x => x.Value).ToList();

                if (dictionaryTable.ContainsKey(lconfigNodeList[column1]))
                {
                    rowList.Add(dictionaryTable);
                    dictionaryTable = new Dictionary<string, string>();
                }

                dictionaryTable.Add(lconfigNodeList[column1], lconfigNodeList[column2]);
            }

            if (dictionaryTable.Count > 0)
                rowList.Add(dictionaryTable);

            return rowList;
        }
    }

    //Таблицы данных c JSON файлов

    public class FpsRow
    {
        public string ITEM_ID; // ID fps объекта
        public string ITEM_NAME; // Наименование fps объекта
        public string HOLD_FIRE; // Одиночная стрельба или нет
        public string FIRE_RATE; // Промежуток между выстрелами
        public string SPREAD; // Разброс выстрелов
        public string BULLET_NUM; // Выстрелов в секунду
        public string DAMAGE; //
        public string DAMAGE_RANGE; //
        public string FORCE;//
        public string MINIGUN; // 
        public string MAX_PENETRATE; // Через сколько объектов проходит выстрел
        public string DISTANCE; // 
        public string AMMO; //
        public string AMMO_MAX; //
        public string AMMO_HAVE; //
        public string CLIP_SIZE; //
        public string DURABILITY_SP; // 
        public string ITEM_USED; //
        public string INFINITY_AMMO; //
        public string WEAPON_TYPE; //
        public string AUTO_ATTACK_TRASHHOLD;
        public string CONTROL_MODIFICATOR;
        public string DELAY_BEFORE_RETURNS_SENS;
        public string FIRE_OFFSET;
        public string FOV;
    }

    public class TranslateRow
    {
        public string ID; // ID строки перевода
        public string STRING; // Строка
        public string SOUND; //Звук
    }

    public class ItemRow
    {
        public string ID; // ID предмета
        public string ITEM_NAME; // имя предмета
        public string ITEM_DESCRIPTION; // Описание предмета
        public string ITEM_DROP_NAME; // имя дропа предмета
        public string ITEM_KEY; // ключ локализации названия предмета
        public string IMAGE_ID; // ID изображения предмета для инвентаря
        public string A_IMAGE_ID; // ID изображения предмета для инвентаря
        public string BROKEN_IMAGE_ID; // ID изображения предмета для инвентаря
        public string MESH_ID; // ID 3д вьюхи, для отображения на сцене.
        public string ITEM_SIZE; // размеры предмета в инвентаре.
        public string STACK; // cобирается ли в стак объект
        public string STACK_SIZE; // размер стака
        public string LIMIT; // размер стака
        public string BELT; // может быть помещен в пояс
        public string AUTO_USE; // автоюзный
        public string ITEM_USE; // можно использовать
        public string ITEM_DESTROY; //время горения
        public string ITEM_EQUIP; // можно экипировать
        public string ITEM_TYPE; // тип предмета
        public string KEY_ID; // ID связанной сущности
        public string ITEM_OPTIONS; // параметр
        public string CHARGE_COUNT; //время горения
    }

    [Serializable]
    public class QuestLaunch
    {
        public int INDEX;
        public string QUEST_ID;
        public string TYPE;
        public string LAUNCH_CONDITIONS;
        public string ACTIVE;
        public string DONE;
    }

    public class RecipesRow
    {
        public string ID; // ID рецепта
        public string NAME; // имя рецепта
        public string INGREDIENTS; // список ингредиентов
        public string AMOUNT; // количество ингредиентов
        public string AMOUNT_RESULT; // полученное итоговое колличество
        public string ID_RESULT; // ID результата
        public string ROUND; // Круг крафта
        public string CRAFT_TIME; //Время крафта
        public string CRAFT_TIME_TEMP; // Темп крафта
        public string CRAFT_ID_OBJECTS; // Список ID объектов на которых возможно крафтить объект
        public string CHARGES_ID; // Список ID объектов на которых возможно крафтить объект
        public string CHARGES_COUNT; // Список ID объектов на которых возможно крафтить объект
        public string PANEL_NUMBER; // номер фильтра
    }

    public class ScreenPlayLaunch
    {
        public string SCENARIO_ID; //ID квеста
        public string LAUNCH_CONDITIONS; // Тип квеста
        public string TYPE; // Условия запуска квеста
        public string DONE; // Уcловия если квест выполнен (YES/NO)
        public string ACTIVE;// Активен квест или нет
        public string SCENARIO_SETTINGS;// Список сценариев
        public int INDEX; // Индекс позиция в таблице
    }

    public class ScreenPlayReaction
    {
        public string SCENARIO_ID; //ID квеста
        public string TYPE_REACTION; // Условия запуска квеста (тип реакций)
        public string MESSAGE_ID; // Тип сообщения (возможно используется для головы)
        public string SETTINGS_REACTION; // Индекс позиция в таблице
        public int INDEX; // Индекс позиция в таблице
    }

    public class ShopRow
    {
        public string GOODSID; // ID товара
        public string IAPID; // ID товара
        public string SHOPTAB; // вкладка
        public string GOODSNAME; //имя товара 
        public string GOODSDESCRIPTION; // описание товара
        public string ITEMID; // id предмета, который получит игрок
        public string ITEMCOUNT; // количество предметов
        public string CURRENCYID; // id предмета валюты
        public string CURRENCYCOUNT; // стоимость
        public string SALESIZE; // размер "скидки"
    }

    public class DailyQuests
    {
        public string ID;
        public string TYPE_QUEST;
        public string GOAL;
        public string GOAL_COUNT;
        public string REWARD;
        public string REWARD_COUNT;
        public string WEIGHT;
        public string CONDITION_BOOK_DONE;
        public string CONDITION_BOOK_NO;
        public string CONDITION_DOOR_OPEN;
        public string CONDITION_DOOR_CLOSE;
        public string CONDITION_TUTOR_DONE;
        public string CONDITION_TUTOR_NO;
    }

    public class LootRow
    {
        public string ID; // ID предмета
        public string ITEM_LIST_ID; // имя предмета
        public string AMOUNT; // Описание предмета
        public string CHANCE; // имя дропа предмета
        public string WEIGHT; // ключ локализации названия предмета
    }

    public class EnemyWave
    {
        public string WaveName;
        public string WaveTime;
        public string SpawnerName;
        public string EnemyName;
        public string EnemyValue;
        public string OverrideAI;
        public string SpawnerPosition;
        public string SpawnerTemp;
        public string WhoAreAttacking;
        public string Settings;
    }

    public class CaseRow
    {
        public string WaveName;
        public string WaveTime;
        public string SpawnerName;
        public string CaseID;
        public string SpawnerPosition;
    }

    public class UnitRow
    {
        public string UNIT_NAME;
        public string SKIN_NAME;
        public string TYPE;
        public string AI_VISION_DIST;
        public string AI_REVIEW_ANGLE;
        public string AI_MEMORY_TIME;
        public string AI_HEARING_DIST;
        public string HEALTH;
        public string NAV_MIN;
        public string NAV_MAX;
        public string NAV_AREA_MASK;
        public string NAV_RADIUS;
        public string NAV_STOP_DIST;
        public string NAV_ANIM_SPEED;
        public string ID_SKILL;
        public string ID_EQUIPMENT;
    }

    public class PortalRow
    {
        public string PORTAL_NAME;
        public string PORTAL_POS;
        public string ACTIVATE_TIME;
        public string ID; // ID строки
        public string DOOR_ID; // ID двери
        public string KEY_ID; // ID ключа
        public string SPRITE_ID; // ID спрайта.
        public string LOC_SPRITE_ID; // ID спрайта.
        public string RESULT_ITEM; // ID ключа
        public string RESULT_COUNT; // ID ключа
        public string PASS_ITEM; // ID предметов для прохода
        public string PASS_ITEM_COUNT; // количество предметов для прохода
        public string DESTROY_KEY; // Удалить ключ после использования
        public string IS_OPEN;
    }

    public class InjuryRow
    {
        public string ID; // ID ущерба
        public string EFFECT_LIST_ID; // список эффектов которые налагает объект
        public string ACCIDENT; // шанс выпадения эффекта %
        public string PARAMETERS; // параметры
        public string SKILLIDS; // id способностей
        public string PREVENTS_ID; // id предметов, которые не дают сработать способности.
        public string PREVENTS_COUNT; // количество уичтожаемых превентов.
        public string EXLUDING_EFFECTS_ID; // id эффектов, которые будут удалены при наложении данного.
        public string EXLUDING_EFFECTS_COUNT; // количество эффектов, которые будут удалены при наложении данного.
        public string BLOCKING_EFFECTS_ID; // id эффектов, которые блокируют наложение данного. ( данная проверка осуществляется раньше, чем превенты)
        public string PIC_ID; // id изображения травмы в баре травм
    }

    public class KeyRow
    {
        public string ID; // ID строки
        public string DOOR_ID; // ID двери
        public string KEY_ID; // ID ключа
        public string SPRITE_ID; // ID спрайта.
        public string RESULT_ITEM; // ID ключа
        public string RESULT_COUNT; // ID ключа
        public string PASS_ITEM; // ID предметов для прохода
        public string PASS_ITEM_COUNT; // количество предметов для прохода
        public string DESTROY_KEY; // Удалить ключ после использования
        public string DESCRIPTION; // Описание ключа
    }

    public class LocationRow
    {
        public string NAME; // имя локации
        public string SCENENAME; // имя сцены
        public string COSTS_ID; // Список ID объектов  - цена
        public string COSTS_GOODS_ID;
        public string COSTS_GOODS_COUNT; // кол-во объектов цены
        public string COSTS_COUNT; // кол-во объектов цены
        public string COSTS_SUITCASE_COUNT; // кол-во объектов цены
        public string COSTS_SUITCASE_ID; // кол-во объектов цены
        public string PIC_ID; // id спрайта
        public string PLACE_COUNT; // количество спавн точек; - невозможно конфигурировать, но позволяет загружать
        public string REPAIR_COSTS_ID; // кол-во объектов цены
        public string REPAIR_COSTS_COUNT; // кол-во объектов цены
    }

    public class ScenaRow
    {
        public string ID; // ID предмета
        public string TYPE; //  Тип активности
        public string SETTINGS; // Условия выполнения активности
        public string SCENE_GOAL; // ID Сцены которая должна выполнится.
        public string LOCATION; //Название локаций
    }

    public class ScenaScenarioRow
    {
        public string ID; // ID предмета
        public string TYPE; //  Тип активности
        public string WEIGHT; // Условия выполнения активности
        public string SCENARIO; // Тип сценария который должен выполнятся
        public string LOCATION; //Название локаций
        public string TO_USE; //Переиспользование сценария  (YES не блокировать повторное использование) (NO блокировать)
    }

    public class ScenaFirstRow
    {
        public string LOCATION; //Локация с которой стартует сцена
        public string SCENARIO; //Начальный сценарий
    }

    [System.Serializable]
    public class QuestLaunchSMH
    {
        public string GOAL_TYPE;
        public string CONDITION;// Тип квеста
        public string TARGET_INFO;
    }

}

