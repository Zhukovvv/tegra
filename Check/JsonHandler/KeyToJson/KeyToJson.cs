﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла keylist
    /// </summary>
    public class KeyToJson : Singleton<KeyToJson>, IJson
    {
        private GetListToResorses<KeyRow> GetListToResorses { get; }
        private List<KeyRow> keytable = new List<KeyRow>();

        private JsonNode jsonNode;
        private string listName = "Key";

        public KeyToJson()
        {
            keytable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.keylist);
        }

        public List<KeyRow> GetKeyTable()
        {
            return keytable;
        }

        //Найти ключ по ID
        public KeyRow GetKeyToId(string doorId)
        {
            KeyRow qwery = new KeyRow();
            try
            {
                qwery = (from KeyRow keyRow in GetKeyTable() where keyRow.DOOR_ID == doorId select keyRow).First();
            }
            catch (InvalidOperationException e)
            {
                //TODO Добавить обработчик
                Debug.Log("Error Key table " + e);
            }

            return qwery;
        }

        //Получить весь смассив Id
        public string[] GetAllDoorId()
        {
            ArrayList doorIdList = new ArrayList();

            foreach (KeyRow keyRow in GetKeyTable())
            {
                doorIdList.Add(keyRow.DOOR_ID);
            }
            return doorIdList.ToArray(typeof(string)) as string[];
        }

    }
}
