﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла locationslist
    /// </summary>
    public class LocationsToJson :Singleton<LocationsToJson>, IJson
    {
        private GetListToResorses<LocationRow> GetListToResorses { get; }
        private List<LocationRow> locationtable = new List<LocationRow>();

        private JsonNode jsonNode;
        private string sheetName = "Round";

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.locationslist);
        }

        public List<LocationRow> GetItemTable()
        {
            locationtable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
            return locationtable;
        }
    }

}
