﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для работы с lootlist
    /// </summary>
    public class LootToJson : Singleton<LootToJson>, IJson
    {
        private GetListToResorses<LootRow> GetListToResorses { get; }
        List<LootRow> loottable = new List<LootRow>();
        JsonNode jsonNode;
        private string sheetName = "Loot";

        public LootToJson()
        {
            loottable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.lootlist);
        }

        public List<LootRow> GetLootTable()
        {
            return loottable;
        }
    }
}
