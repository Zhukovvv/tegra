﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс данных для работы с portalList
    /// </summary>
    public class PortalToJson : Singleton<PortalToJson>, IJson
    {
        private GetListToResorses<PortalRow> GetListResorses { get; set; }
        List<PortalRow> portaltable = new List<PortalRow>();

        JsonNode jsonNode;
    
        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.portallist);
        }

        public void PortalDataToJson(string sheetName)
        {
            var jsonData = GetJsonData();
            portaltable = GetListResorses.GetSheetToList(jsonData, sheetName);
        }

        public List<PortalRow> GetPortalTable()
        {
            return portaltable;
        }

        public List<string> GetSheetList()
        {
            var jsonData = jsonNode.GetJsonTextAsset(JsonList.portallist);
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData.text);
            return values.Keys.ToList();
        }
    }

}
