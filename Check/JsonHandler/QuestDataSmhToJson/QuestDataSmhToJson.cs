﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла questlist для smh
    /// </summary>
    public class QuestDataSmhToJson : Singleton<QuestDataSmhToJson>, IJson
    {
        private GetListToResorses<QuestLaunchSMH> GetListToResorses { get; }

        JsonNode jsonNode;

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.questlist);
        }

        /// <summary>
        /// The Init
        /// </summary>
        public void Init(string name, ref QuestLaunchConfig questLaunchConfig)
        {
            var smhtables = GetListToResorses.GetSheetToList(GetJsonData(), name);

            questLaunchConfig.dataSMH = new List<QuestLaunchSMH>();

            foreach (QuestLaunchSMH smhtable in smhtables)
            {
                var questRow = new QuestLaunchSMH()
                {
                    GOAL_TYPE = smhtable.GOAL_TYPE,
                    CONDITION = smhtable.CONDITION,
                    TARGET_INFO = smhtable.TARGET_INFO,
                };

                questLaunchConfig.dataSMH.Add(questRow);
            }
        }
    }
}
