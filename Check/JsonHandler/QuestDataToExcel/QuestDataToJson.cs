﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleJSONMetaSheets;
using UnityEngine;

/// <summary>
/// Класс для работы с questlist
/// </summary>
namespace JsonConvertor
{
    public class QuestDataToJson : Singleton<QuestDataToJson>, IJson
    {
        private GetListToResorses<QuestLaunchConfig> GetListToResorses { get; }
        List<QuestLaunchConfig> questtable = new List<QuestLaunchConfig>();
        JsonNode jsonNode;

        string listName = "Quest";

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.questlist);
        }

        public QuestDataToJson()
        {
            questtable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
            InitDataToSaveManager();
        }

        /// <summary>
        /// The GetQuestRow
        /// </summary>
        /// <returns>The <see cref="List{QuestRow}"/></returns>
        public List<QuestLaunchConfig> GetQuestRow()
        {
            return SaveManager.Instance.saveCell.questLaunchConfigList;
        }

        /// <summary>
        /// Получение данных только для едитора
        /// </summary>
        /// <returns></returns>
        public List<QuestLaunchConfig> GetListRow()
        {
            return GetListToResorses.GetSheetToList(GetJsonData(), listName);
        }

        private void InitDataToSaveManager()
        {
            //if (SaveManager.Instance.saveCell.questLaunchConfigList.Count == 0)
            //{
                SaveManager.Instance.saveCell.questLaunchConfigList = new List<QuestLaunchConfig>();

                if (questtable.Count > 0)
                {
                    for (int i = 0; i <= questtable.Count - 1; i++)
                    {
                        var questRow = questtable[i];

                        QuestDataSmhToJson.Instance.Init(questRow.ID, ref questRow);
                        SaveManager.Instance.saveCell.questLaunchConfigList.Add(questRow);
                    }
                }
           // }
        }

        public string GetIdToQuestGoal(string questCoal)
        {
            List<QuestLaunchConfig> list = Instance.GetQuestRow();
            string query = (from QuestLaunchConfig row in list where row.QUEST_GOAL == questCoal select row.ID).First().ToString();
            return query;
        }

        /// <summary>
        /// The GetTypeQuest
        /// </summary>
        /// <param name="LaunchType">The LaunchType<see cref="string"/></param>
        /// <returns>The <see cref="List{QuestLaunch}"/></returns>
        /// 
        public ArrayList GetTypeQuest(string typeQuest)
        {
            List<QuestLaunchConfig> list = Instance.GetListRow();
            ArrayList tempArray = new ArrayList();

            var query = (from QuestLaunchConfig row in list where row.TYPE_QUEST == typeQuest select row.QUEST_GOAL).ToList();

            foreach (var row in query)
            {
                tempArray.Add(row);
            }

            return tempArray;
        }

        //Найти строку по Id
        /// <summary>
        /// The GetRowId
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="QuestLaunchConfig"/></returns>
        public QuestLaunchConfig GetRowId(string id)
        {
            QuestLaunchConfig qwery;
            try
            {
                qwery = (from QuestLaunchConfig row in GetQuestRow() where row.ID == id select row).First();
                return qwery;
            }
            catch (Exception e)
            {
                //TODO Добавить обработчик
                Debug.Log("Ошибка в таблице не найден id  " + id + "" + e);
            }

            return null;
        }

    }
}
