﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleJSONMetaSheets;
using UnityEngine;

/// <summary>
/// Класс для работы с questlist
/// </summary>
namespace JsonConvertor
{
    /// <summary>
    /// Defines the <see cref="QuestLaunchToJson" />
    /// </summary>
    public class QuestLaunchToJson : Singleton<QuestLaunchToJson>, IJson
    {
        private GetListToResorses<QuestLaunch> GetListToResorses { get; }
        List<QuestLaunch> launchtable = new List<QuestLaunch>();
        JsonNode jsonNode;

        string listName = "Launch";

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.questlist);
        }

        public QuestLaunchToJson()
        {
            launchtable = GetListToResorses.GetSheetToList(GetJsonData(), listName);
            InitDataToSaveManager();
        }

        /// <summary>
        /// The GetLaunchRow
        /// </summary>
        /// <returns>The <see cref="List{QuestLaunch}"/></returns>
        public List<QuestLaunch> GetLaunchRow()
        {
            return SaveManager.Instance.saveCell.questLaunchList;
        }

        private void InitDataToSaveManager()
        {
            if (SaveManager.Instance.saveCell.questLaunchList.Count == 0)
            {
                SaveManager.Instance.saveCell.questLaunchList = new List<QuestLaunch>();

                if (launchtable.Count > 0)
                {
                    for (int i = 0; i <= launchtable.Count - 1; i++)
                    {
                        var questRow = launchtable[i];
                        questRow.DONE = "NO";
                        questRow.ACTIVE = "NO";
                        SaveManager.Instance.saveCell.questLaunchList.Add(questRow);
                    }
                }
            }
        }

        /// <summary>
        /// The GetAllLaunchType
        /// </summary>
        /// <param name="LaunchType">The LaunchType<see cref="string"/></param>
        /// <returns>The <see cref="List{QuestLaunch}"/></returns>
        public List<QuestLaunch> GetAllLaunchType(string LaunchType)
        {
            IEnumerable<QuestLaunch> query = null;
            try
            {
                query = (from QuestLaunch row in GetLaunchRow() where row.LAUNCH_CONDITIONS == LaunchType select row);
                return query.ToList();
            }
            catch (Exception e)
            {
                //TODO Добавить обработчик
                Debug.Log("Ошибка в таблице не найден LAUNCH_CONDITIONS" + e);
            }

            return query.ToList();
        }

        //Найти строку по Id
        /// <summary>
        /// The GetRowId
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="QuestLaunch"/></returns>
        public QuestLaunch GetRowId(string id)
        {
            QuestLaunch query = new QuestLaunch();

            try
            {
                query = new QuestLaunch();

                query = (from QuestLaunch row in GetLaunchRow() where row.QUEST_ID == id select row).First();
                return query;
            }
            catch (Exception e)
            {
                //TODO Добавить обработчик
                //Debug.Log("Ошибка в таблице не найден id" + e + " " + id);
            }

            return query;
        }

        //Наити Id в таблице и поставить условия что квест выполнен
        /// <summary>
        /// The QuestComplite
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <param name="rowName">The rowName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public void QuestComplite(string id, string rowName, string message)
        {
            //SaveSheet(id, rowName, message);
            SaveSheetToSaveManager(id, rowName, message);
        }

        /// <summary>
        /// The SaveSheet
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <param name="rowName">The rowName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public void SaveSheetToSaveManager(string id, string rowName, string message)
        {
            var row = GetRowId(id);
            switch (rowName)
            {
                case "DONE":
                    {
                        row.DONE = message;
                    }
                    break;
                case "ACTIVE":
                    {
                        row.ACTIVE = message;
                    }
                    break;
            }
        }
    }
}
