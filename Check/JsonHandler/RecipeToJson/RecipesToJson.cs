﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с recipelist
    /// </summary>
    public class RecipesToJson : Singleton<RecipesToJson>
    {
        private GetListToResorses<RecipesRow> GetListToResorses { get; }
        private List<RecipesRow> recipestable = new List<RecipesRow>();
        private JsonNode jsonNode;
        public string sheetName = "Round";

        public JSONNode GetJsonData(string sheetName)
        {
            return jsonNode.GetJsonNode(sheetName);
        }

        public List<RecipesRow> GetRecipeTable(string excel)
        {
            recipestable = GetListToResorses.GetSheetToList(GetJsonData(excel), sheetName);
            return recipestable;
        }   
    }
}
