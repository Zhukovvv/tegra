﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Общий класс  для scenalist
    /// </summary>
    public class ScenaListHandler<T> where T : new()
    {   
        private GetListToResorses<T> GetListToResorses { get; set; }

        private List<T> scenariotable = new List<T>();
        private JsonNode jsonNode;

        public ScenaListHandler(string sheetName)
        {
            scenariotable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.scenelist);
        }

        public List<T> GetScenaTable()
        {
            return scenariotable;
        }
    }
}
