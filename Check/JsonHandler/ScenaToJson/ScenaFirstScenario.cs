﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла scenalist
    /// </summary>
    public class ScenaFirstScenario : Singleton<ScenaFirstScenario>, IJson
    {
        private string sheetName = "FirstScenario";
        ScenaListHandler<ScenaFirstRow> scenaBehavior { get; }

        public ScenaFirstScenario()
        {
            scenaBehavior = new ScenaListHandler<ScenaFirstRow>(sheetName);
        }

        public JSONNode GetJsonData() => scenaBehavior.GetJsonData();

        public List<ScenaFirstRow> GetScenaTable() => scenaBehavior.GetScenaTable();
    }
}
