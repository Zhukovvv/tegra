﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла scenalist
    /// </summary>
    public class ScenaToJson : Singleton<ScenaToJson>, IJson
    {
        private string sheetName = "Launch";
        ScenaListHandler<ScenaRow> scenaBehavior { get; }

        public ScenaToJson()
        {
            scenaBehavior = new ScenaListHandler<ScenaRow>(sheetName);
        }

        public JSONNode GetJsonData() => scenaBehavior.GetJsonData();
      
        public List<ScenaRow> GetScenaTable() => scenaBehavior.GetScenaTable();
       
    }
}
