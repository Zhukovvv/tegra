﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла scenalist
    /// </summary>
    public class ScenarioToJson : Singleton<ScenarioToJson>, IJson
    {
        private string sheetName = "Scenario";
        ScenaListHandler<ScenaScenarioRow> scenaBehavior { get; }

        public ScenarioToJson()
        {
            scenaBehavior = new ScenaListHandler<ScenaScenarioRow>(sheetName);
        }

        public JSONNode GetJsonData() => scenaBehavior.GetJsonData();

        public List<ScenaScenarioRow> GetScenaTable() => scenaBehavior.GetScenaTable();
    }
}
