﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для работы с реакциями для screenplaylist
    /// </summary>
    public class ScreenPlayReactionToJson : Singleton<ScreenPlayReactionToJson>, IJson
    {
        private GetListToResorses<ScreenPlayReaction> GetListToResorses { get; }
        private List<ScreenPlayReaction> screeplaytable = new List<ScreenPlayReaction>();
        private JsonNode jsonNode;

        public ScreenPlayReactionToJson()
        {
            JSONNode jsonData = GetJsonData();

            foreach (string sheetName in GetListToResorses.GetSheetListName(jsonData))
            {
                screeplaytable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
            }
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.reactionlist);
        }

        public List<ScreenPlayReaction> GetReactionRow()
        {
            return screeplaytable;
        }
    }
}
