﻿using System.Collections.Generic;
using SimpleJSONMetaSheets;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для получения данных с файла screenplaylist
    /// </summary>
    public class ScreenPlayToJson : Singleton<ScreenPlayToJson>, IJson
    {
        private GetListToResorses<ScreenPlayLaunch> GetListToResorses { get; }
        private List<ScreenPlayLaunch> screeplaytable = new List<ScreenPlayLaunch>();
        private JsonNode jsonNode;
        
        public ScreenPlayToJson()
        {
            JSONNode jsonData = GetJsonData();

            foreach (string sheetName in GetListToResorses.GetSheetListName(jsonData))
            {
               screeplaytable.AddRange(GetListToResorses.GetSheetToList(jsonData, sheetName));
            }
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.screenplaylist);
        }

        public List<ScreenPlayLaunch> GetLaunchRow()
        {
            return screeplaytable;
        }
    }
}
