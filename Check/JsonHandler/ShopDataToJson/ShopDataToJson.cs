﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSONMetaSheets;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс для работы с данными shoplist
    /// </summary>
    public class ShopDataToJson : IJson
    {
        private GetListToResorses<ShopRow> GetListToResorses { get; }
        private List<ShopRow> shopTable = new List<ShopRow>();
        private JsonNode jsonNode;

        public ShopDataToJson(string sheetName)
        {
            InitShopList(sheetName);
        }

        private void InitShopList(string sheetName)
        {
            shopTable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.shoplist);
        }

        public List<ShopRow> GetShopTable()
        {
            return shopTable;
        }
    }

}
