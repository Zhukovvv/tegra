﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SimpleJSONMetaSheets;
using UnityEngine;

/// <summary>
/// Класс для получения данных с translatelist
/// </summary>
namespace JsonConvertor
{
    public class TranslateToJson : Singleton<TranslateToJson>, IJson
    {
        private GetListToResorses<TranslateRow> GetListToResorses { get; }
        private List<TranslateRow> translateTable = new List<TranslateRow>();
        private JsonNode jsonNode;
        public string sheetName = "EN";


        public TranslateToJson()
        {
            UpdateTranslateTable();
        }

        public void InitTranslateList(string sheetName)
        {
            this.sheetName = sheetName;
            UpdateTranslateTable();
        }

        private void UpdateTranslateTable()
        {
            translateTable = new List<TranslateRow>();
            translateTable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.translatelist);
        }

        public List<string> GetSheetList()
        {
            var jsonData = jsonNode.GetJsonTextAsset(JsonList.translatelist);
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData.text);
            return values.Keys.ToList();
        }

        //Найти строку по Id
        public TranslateRow GetTranslateString(string stringID)
        {
            TranslateRow qwery = new TranslateRow();
            try
            {
                qwery = (from TranslateRow row in translateTable where row.ID == stringID select row).FirstOrDefault();

                if (qwery.STRING == "" || qwery.STRING == null)
                    return qwery;

            }
            catch (Exception e)
            {
                //Debug.Log("Id Error " + stringID);
            }

            return qwery;
        }
    }
}
