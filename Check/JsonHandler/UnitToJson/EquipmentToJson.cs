﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс данных для работы с Equipmentlist
    /// </summary>
    public class EquipmentToJson : Singleton<EquipmentToJson>
    {
        JsonNode jsonNode;

        GetUnitConfig GetUnitConfig;

        public Dictionary<string, ArrayList> GetSheetList()
        {
            TextAsset jsonData = jsonNode.GetJsonTextAsset(JsonList.equipmentlist);
            return JsonConvert.DeserializeObject<Dictionary<string, ArrayList>>(jsonData.text);
        }

        public ArrayList SelectRowList(string sheetName)
        {
            var nodeData = GetSheetList();
            return GetUnitConfig.SelectRowList(sheetName, nodeData);
        }
    }
}
