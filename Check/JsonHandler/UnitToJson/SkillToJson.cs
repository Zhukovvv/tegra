﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace JsonConvertor
{
    /// <summary>
    /// Класс данных для работы с Skilllist
    /// </summary>
    public class SkillToJson : Singleton<SkillToJson>
    {
        JsonNode jsonNode;

        GetUnitConfig GetUnitConfig;

        public Dictionary<string, ArrayList> GetSheetList()
        {
            TextAsset jsonData = jsonNode.GetJsonTextAsset(JsonList.skilllist);
            return JsonConvert.DeserializeObject<Dictionary<string, ArrayList>>(jsonData.text);
        }

        public ArrayList SelectRowList(string sheetName)
        {
            var nodeData = GetSheetList();
            return GetUnitConfig.SelectRowList(sheetName, nodeData);
        }
    }
}
