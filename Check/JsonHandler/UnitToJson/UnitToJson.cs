﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using SimpleJSONMetaSheets;
using UnityEngine;
/// <summary>
/// Класс данных для работы с unitlist
/// </summary>
namespace JsonConvertor
{
    public class UnitToJson : Singleton<UnitToJson>, IJson
    {
        private GetListToResorses<UnitRow> GetListToResorses { get; }
        private List<UnitRow> unitTable = new List<UnitRow>();
        private string sheetName = "Unit";
        private JsonNode jsonNode;

        public UnitToJson()
        {
            unitTable = GetListToResorses.GetSheetToList(GetJsonData(), sheetName);
        }

        public JSONNode GetJsonData()
        {
            return jsonNode.GetJsonNode(JsonList.unitlist);
        }

        public List<UnitRow> GetUnitTable()
        {
            return unitTable;
        }

        //Найти по имени
        public UnitRow GetUnitConfiguration(string unitName)
        {
            UnitRow unitRow = new UnitRow();

            // Define the query expression.
            foreach (var unit in GetUnitTable())
            {
                if (unit.UNIT_NAME == unitName)
                {
                    return unit;
                }
            }

            return unitRow;
        }
    }
}
