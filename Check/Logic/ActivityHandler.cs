﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Впомогательный класс для активностей сценариев
/// </summary>
public class ActivityHandler : MonoBehaviour, IActivity
{
    public string ActivityId = null;

    private void Awake()
    {
        //Инициализировать очередь выполненых активностей
        ActivitySaver.Instance.InitActivityData(SceneManager.GetActiveScene().name);
        LockUsedObjectName();
    }

    //Получить полное имя все родственных объектов
    private string GetFullNameObject()
    {
        string parent = "";

        var tmp = transform;
        while (tmp)
        {
            parent += tmp.name + ":";
            tmp = tmp.parent;
        }

        // ":" для разделения объектов на проверку только для отладки (не убирать)

        Regex rgx = new Regex("[^a-zA-Z0-9]");
        return rgx.Replace(parent, "");
    }

    //Сохранить полное имя объекта активированной активности
    public void SaveUsedActivity()
    {
        string fullName = GetFullNameObject();

        if ("" != fullName)
        {  
            //Сохранить полное имя объекта активности
            ActivitySaver.Instance.SaveFullNameActivity(fullName);

            //Сохранить в очереди имя выполненой активности
            ActivitySaver.Instance.SaveUsedActivity(ActivityId);
        }
    }

    //Проверить имя активности (заблокировать если уже использовалось)
    public void LockUsedObjectName()
    {
        string fullName = GetFullNameObject();

        if (ActivitySaver.Instance.CheckFullName(fullName) || ActivitySaver.Instance.CheckUsedActivity(ActivityId))
        {
            gameObject.SetActive(false);
        }
    }
}
