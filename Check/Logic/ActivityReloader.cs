﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс перегружает все активности на уровне
/// </summary>
public class ActivityReloader : MonoBehaviour
{
    private List<string> activityList = null;

    public static void RunReload()
    {
        //Перегрузить очередь выполненых активностей
        var activityList = ActivitySaver.Instance.GetUsedActivityList();

        GameObject reloader = new GameObject();   
        reloader.AddComponent<ActivityReloader>().activityList = activityList;
    }

    private void Start()
    {
        StartCoroutine(Reload(activityList));
    }

    public IEnumerator Reload(List<string> activityList)
    {
        foreach (var activityId in activityList)
        {
            ScreenPlayActivityManager.RunActivity(activityId, true);
            yield return 2;
        }

        Destroy(gameObject);
    }
}
