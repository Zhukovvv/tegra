﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

//Структура сохраненных данных активностей
[Serializable]
public class ActivityData
{
    //Название локаций к которой относится активность
    public string locationName { get; }

    //Ид сценария
    public string scenarioId { get; set; }

    public ActivityData(string locationName, string scenarioId)
    {
        this.locationName = locationName;
        this.scenarioId = scenarioId;
    }

    //Очередить вызовов активностей
    public Queue<string> ActivityQueue = new Queue<string>();

    //Очередь имене активностей
    public ArrayList ActivityNameList = new ArrayList();

    //Таблица статуса активностей
    public Dictionary<string, bool> CompliteReactionTable = new Dictionary<string, bool>();

    //Таблица выполненых волн
    public List<string> CompliteWaveNameList = new List<string>();

    //Таблица колличества убитых ботов
    public Hashtable KillValueUnitTable = new Hashtable();
}

/// <summary>
/// Класс сохранения выполненых активностей
/// </summary>
public class ActivitySaver : Singleton<ActivitySaver>
{
    private ActivityData activityData = null;
    public Action onClearActivityList;
    private List<string> boxInScenario = new List<string>();
    private List<string> itemPickupInScenario = new List<string>();
    private bool isAlreadyDone;


    /// <summary>
    /// Инициализировать класс очереди выполненых объектов
    /// </summary>
    public void InitActivityData(string sceneName)
    {
        //Получить id активного сценария
        string scenarioId = ScenaStorage.GetDataToStorage(SceneManager.GetActiveScene().name);

        if (activityData == null || activityData.locationName != SceneManager.GetActiveScene().name || activityData.scenarioId != scenarioId)
        {
            InitScenario.updateScenarioPrefab?.Invoke();
            UpdateActivityData(SceneManager.GetActiveScene().name, scenarioId);
            isAlreadyDone = false;
        }
    }

    //Обновить класс очереди выполненых объектов
    private void UpdateActivityData(string sceneName, string scenarioId)
    {
        activityData = new ActivityData(sceneName, scenarioId);

        string keyString = string.Concat("ActivityData", sceneName, scenarioId);

        string binaryString = SaveManager.Instance.GetSave(keyString);

        if (binaryString != "")
        {
            activityData = DeserializeObject<ActivityData>(binaryString);
        }
    }

    //Сохранить имя выполненой активности
    public void SaveUsedActivity(string activityId)
    {
        if (activityId != "")
        {
            if (!activityData.ActivityQueue.Contains(activityId))
                activityData.ActivityQueue.Enqueue(activityId);
            SerializeQueueObject(activityData);
        }
    }

    //Получить из списка данные об активности
    public bool CheckUsedActivity(string activityId)
    {
        return activityData.ActivityQueue.Contains(activityId);
    }

    //Получить список выполненых активностей
    public List<string> GetUsedActivityList()
    {
        var queue = activityData.ActivityQueue.ToList();
        return queue;
    }

    //Сохранить запущенную реакцию
    public void AddListRunReaction(string reactionKey)
    {
        if (reactionKey != "")
        {
            if (!activityData.CompliteReactionTable.ContainsKey(reactionKey))
                activityData.CompliteReactionTable.Add(reactionKey, true);
        }
    }

    //Пометить реакцию как выполненую
    public void LockCompliteReaction(string reactionKey)
    {
        if (reactionKey != "")
        {
            if (activityData.CompliteReactionTable.ContainsKey(reactionKey))
            {
                activityData.CompliteReactionTable[reactionKey] = false;
                SerializeQueueObject(activityData);
            }
        }
    }

    //Получить статус реакция
    public bool GetStatusReaction(string reactionKey)
    {
        if (activityData.CompliteReactionTable.ContainsKey(reactionKey))
            return activityData.CompliteReactionTable[reactionKey];

        return true;
    }

    //Добавить в список выполненых волн на уровне
    public void SaveCompliteWaveName(string waveName)
    {
        activityData.CompliteWaveNameList.Add(waveName);
    }

    //Получить из списка выполненую волну
    public bool CheckCompliteWaveName(string waveName)
    {
        return activityData.CompliteWaveNameList.Contains(waveName);
    }

    //Добавить в таблицу колличеcтво убитых ботов
    public void SaveKilledUnit(string name)
    {
        if (activityData.KillValueUnitTable.ContainsKey(name))
        {
            activityData.KillValueUnitTable[name] = Int32.Parse(activityData.KillValueUnitTable[name].ToString()) + 1;
        }
        else
        {
            activityData.KillValueUnitTable.Add(name, 1);
        }
    }

    //Получить из таблицы колличество убитых ботов
    public int GetValueKilledUnit(string name)
    {
        if (activityData.KillValueUnitTable.ContainsKey(name))
            return Int32.Parse(activityData.KillValueUnitTable[name].ToString());
        return 0;
    }

    //Записать в очередь полной имя активности
    public void SaveFullNameActivity(string fullName)
    {
        activityData.ActivityNameList.Add(fullName);
    }

    //Проверить имя активности в списке
    public bool CheckFullName(string fullName)
    {
        if (activityData.ActivityNameList.Contains(fullName))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Очистить данные о выполненом сценарий
    public void ClearScenarioData()
    {
        activityData.ActivityQueue.Clear();
        activityData.CompliteReactionTable.Clear();
        activityData.CompliteWaveNameList.Clear();
        activityData.KillValueUnitTable.Clear();
        activityData.ActivityNameList.Clear();
        onClearActivityList?.Invoke();
        ClearLoot();
        SerializeQueueObject(activityData);
        isAlreadyDone = true;
    }

    public bool GetStatus()
    {
        return isAlreadyDone;
    }

    #region Сохранение Лута



    public bool AddBox(string id)
    {
        if (!boxInScenario.Contains(id))
        {
            boxInScenario.Add(id);
            return true;
        }
        return false;
    }

    public bool RemoveBox(string id)
    {
        if (boxInScenario.Contains(id))
        {
            boxInScenario.Remove(id);
            return true;
        }
        return false;
    }

    public bool AddPickup(string id)
    {
        if (!itemPickupInScenario.Contains(id))
        {
            itemPickupInScenario.Add(id);
            return true;
        }
        return false;
    }

    public bool RemovePickup(string id)
    {
        if (itemPickupInScenario.Contains(id))
        {
            itemPickupInScenario.Remove(id);
            return true;
        }
        return false;
    }


    public void ClearLoot()
    {
        for (int i = 0; i < boxInScenario.Count; i++)
        {
            SaveManager.Instance.DeleteSave(boxInScenario[i] + "isWasInit");
            SaveManager.Instance.DeleteSave(boxInScenario[i]);
            SaveManager.Instance.DeleteSave(boxInScenario[i] + "lifeTime");
            SaveManager.Instance.DeleteSave(boxInScenario[i] + "isWasOpen");
            SaveManager.Instance.DeleteSave(boxInScenario[i] + "isWasDestroy");
        }

        for (int i = 0; i < itemPickupInScenario.Count; i++)
        {
            SaveManager.Instance.DeleteSave(itemPickupInScenario[i].Replace(":", ""));
            SaveManager.Instance.DeleteSave(itemPickupInScenario[i].Split(':')[1] + "isWasInit");
            SaveManager.Instance.DeleteSave(itemPickupInScenario[i].Split(':')[1]);
        }
        boxInScenario.Clear();
        itemPickupInScenario.Clear();
    }


    #endregion

    #region сериализация объектов
    //Сереализовать объект и записать данные в SaveManager
    private void SerializeQueueObject(ActivityData objectToSerialize)
    {
        string sceneName = SceneManager.GetActiveScene().name;

        if (objectToSerialize.locationName == sceneName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memStr = new MemoryStream();

            try
            {
                bf.Serialize(memStr, objectToSerialize);
                memStr.Position = 0;

                string keyString = string.Concat("ActivityData", sceneName, objectToSerialize.scenarioId);
                SaveManager.Instance.AddSave(keyString, Convert.ToBase64String(memStr.ToArray()));
            }
            finally
            {
                memStr.Close();
            }
        }
    }

    //Десерилизовать объект
    private T DeserializeObject<T>(string str)
    {
        byte[] b = Convert.FromBase64String(str);
        using (var stream = new MemoryStream(b))
        {
            var formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            try
            {
                return (T)formatter.Deserialize(stream);
            }
            finally
            {
                stream.Close();
            }
        }
    }
    #endregion
}
