﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Интерфейс сохранения использованной активности
/// </summary>
public interface IActivity { void SaveUsedActivity();}
