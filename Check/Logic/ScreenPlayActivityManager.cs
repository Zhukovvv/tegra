﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using JsonConvertor;

/// <summary>
/// Вспомагательный класс для работы с данными ScreenPlayActivityManager
/// </summary>
public class ScreenPlayHandler : MonoBehaviour
{
    //Получить таблицу активностей
    private static List<ScreenPlayLaunch> GetLaunchRow()
    {
        return ScreenPlayToJson.Instance.GetLaunchRow();
    }

    //Получить список активностей
    public static ArrayList GetActivity(string activityName)
    {
        var query = from ScreenPlayLaunch row in GetLaunchRow() where row.LAUNCH_CONDITIONS == activityName select row.SCENARIO_ID;

        ArrayList tempArray = new ArrayList();
        foreach (string screenPlayLaunch in query)
            tempArray.Add(screenPlayLaunch);

        return tempArray;
    }

    //Получить параметры активности по id
    public static string GetTypeActivity(string activityId)
    {
        try
        {
            var query = (from ScreenPlayLaunch row in GetLaunchRow() where row.SCENARIO_ID == activityId select row.TYPE).First();
            return query;
        }
        catch (Exception e)
        {
            Debug.Log("Не найден сценарий  " + activityId + "   " + e);
            return null;
        }

    }

    //Получить список настроек по Id
    public static string GetSettingsActivity(string activityId)
    {
        try
        {
            var query = (from ScreenPlayLaunch row in GetLaunchRow() where row.SCENARIO_ID == activityId select row.SCENARIO_SETTINGS).First();
            return query;
        }
        catch (Exception e)
        {
            Debug.Log(" Не найден сценарий  " + activityId + "  " + e);
            return null;
        }
    }
}


/// <summary>
/// Класс обработки данных активностей для сценариев (screenplaylist.xls)
/// </summary>
public class ScreenPlayActivityManager : ScreenPlayHandler
{
    //Активировать активности
    public static void ActivateActivity<T>(string activityId, T activityOb)
    {
        //Сохранить имя использованной активности
        (activityOb as IActivity).SaveUsedActivity();
        RunActivity(activityId, false);
    }

    /// <summary>
    /// Выполнить активности
    /// </summary>
    /// <param name="activityId"></param>
    /// <param name="filtredList">фильтр запрещенных реакций</param>
    public static void RunActivity(string activityId, bool replay)
    {
        //Проверка позволяет выполнить несколько реакций из списка. (SCENARIO_SETTINGS - Launch таблица)
        string stringListScenario = GetSettingsActivity(activityId);

        if (stringListScenario != null)
        {
            foreach (string scenarioId in stringListScenario.Split(':'))
            {
                RunReaction(scenarioId, activityId, replay);
            }

            //Метрика
            try { QuestMetrica.InitQuestMetrica(activityId); } catch (Exception) { };
        }
    }


    /// <summary>
    /// Запустить реакцию
    /// </summary>
    /// <param name="reactionName"></param>
    /// <param name="reactionId"></param>
    public static void RunReaction(string reactionId, string activityId, bool replay = false)
    {
        string settingsReaction = ScreenPlayReactionManager.GetReactionSettings(reactionId);
        string reactionName = ScreenPlayReactionManager.GetReactionName(reactionId);
        string key = activityId + reactionId;

        try
        {
            switch (reactionName)
            {
                case "SPAWNER":
                    WaveReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "PREFAB":
                    PrefabReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "CUT":
                    CutReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "HEAD":
                    HeadReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "ITEM":
                    ItemReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "DELETEITEM":
                    DeleteItemReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "QUEST":
                    QuestStartReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "QUESTEND":
                    QuestEndReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "KILL_BOT":
                    BotKillReaction.Instance.Init(key, settingsReaction, replay);
                    break;
                case "END_SCENARIO":
                    EndScenarioReaction.Instance.Init(key, activityId, replay);
                    break;
                default:
                    ErrorHelper.Instance.FormError("------ Error ReactionName ------" + reactionName);
                    break;
          
                    }

            TegraLog.Loger.Instance.SaveLog(TegraLog.LogStatus.Warning, TegraLog.LogEvent.QuestEnd, reactionName);

        }
        catch (Exception e)
        {
            ErrorHelper.Instance.FormError(reactionName + "------ Error ReactionName  -------" + e.Message);
        }
    }
}
