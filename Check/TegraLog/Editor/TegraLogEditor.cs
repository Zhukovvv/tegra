﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Diagnostics;

public class TegraLogEditor : EditorWindow
{
    [MenuItem("Tools/Tegra <=> Log")]
    static void Apply()
    {
        Text text = Selection.activeObject as Text;

        string logPath = Application.persistentDataPath + "/PerformanceLogs/";
        string path = EditorUtility.OpenFilePanel("Overwrite with png", logPath, "log");

        if (path.Length != 0)
        {
            Process.Start(path);
        }
    }
}

#endif
