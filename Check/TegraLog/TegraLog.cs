﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using UnityEngine;

/// <summary>
/// Класс логер для сохранения данных о логах
/// </summary>
public class TegraLog : MonoBehaviourSingleton<TegraLog>
{
    //Важность лога
    public enum LogStatus
    {
        None = 0,
        Normal, //Пометить лог как нормальный
        Warning,
        Error, //Пометить лог как ошибка
    }

    //События лога
    public enum LogEvent
    {
        None = 0,
        TutorStart,
        TutorEnd,
        QuestStart,
        QuestEnd,
        LocationTransition, //Переход между локациями
        LocationStartSession, //На какой локаций началась сессия
        LocationEndSession,//На какой локаций завершилась сессия
        PurchaseLog, // Лог покупок
        SendContainerLog,//Лог отправки контейнера
        WhoWasKilled, // Кого убили
        WhenDied, // Когда умерли
        WhatCraft, //Что скрафтили 
    }

    //Основной класс логирования
    public class Loger : Singleton<Loger>
    {
        private string folder = Application.persistentDataPath + "/PerformanceLogs/";
        private string archiveFileName { get; }

        public Loger()
        {
            archiveFileName = GetCurrentTime().ToString("hh_mm_ss_") + "TergaExample.log";
        }

        public void SaveLog(LogStatus logStatus, LogEvent logEvent, string log)
        {
#if TEST_VERSION
            try
            { Save(logStatus.ToString(), logEvent.ToString(), log); }
            catch (Exception) { }
#endif
        }

        public void Save(string logStatus, string logEvent, string log)
        {
            string dataString = " ";
            dataString += logStatus + " : ";
            dataString += logEvent + " : ";
            dataString += log + " : ";
            dataString += GetCurrentTime() + " : ";
            dataString += GetMemoryLog() + "Mb";

            Thread dumpThread = new Thread(new ThreadStart(() => WriteDumpLog(dataString, folder)));
            dumpThread.IsBackground = true;
            dumpThread.Start();
        }

        public void WriteDumpLog(string dataString, string folder)
        {
            //Writes data to file         
            StreamWriter logFile = null;

            try
            {
                string path = String.Concat(folder, archiveFileName);
                string directoryPath = System.IO.Path.GetDirectoryName(path);

                if (!Directory.Exists(directoryPath)) { Directory.CreateDirectory(directoryPath); }
                logFile = new StreamWriter(path, true);
                logFile.WriteLine(dataString);
                logFile.Close();
            }

            catch (Exception) { }
        }

        public DateTime GetCurrentTime()
        {
            DateTime localDate = DateTime.Now;
            var culture = new CultureInfo("ru-RU");
            localDate.ToString(culture);
            return localDate;
        }

        public string GetMemoryLog()
        {
            return (GC.GetTotalMemory(false) / (1024 * 1024)).ToString();
        }
    }
}
